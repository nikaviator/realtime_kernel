# Realtime Kernel for Ubuntu 18.4

These are `deb` packages built for Ubuntu Bionic. The packages are created following advice from [Stackoverflow](https://stackoverflow.com/questions/51669724/install-rt-linux-patch-for-ubuntu) and the [Panda manuals](https://frankaemika.github.io/docs/installation_linux.html#setting-up-the-real-time-kernel).

## Information

The kernel is built automatically by the CI from the [linux kernel sources](https://mirrors.edge.kernel.org/pub/linux/kernel/v5.x/) version 5.4, using the latest kernel version for which a [real time patch](https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/5.4/) is available. For more information on how the kernel is built, please review the `compile.bash` script.

It's `config` file is based on the config provided by a Unbuntu Bionic installation with two changes:
- The `preemption model` is set to `Fully Preemptible Kernel (RT)`.
- `Compile the kernel with debug info (DEBUG_INFO)` is set to `no`.

Before the build, if the new kernel version contains new config keys, the corresponding values are set to their defaults.

**⚠ ATTENTION** The packages are designed for **Ubuntu 18.4** only. While they may also work on newser versions of Ubuntu, we recommend to compile them yourself.
You can use the provided shell script, which (using Ubuntu) reuqires the following packages `build-essential curl bison flex bc rsync kmod cpio lsb-release libssl-dev`.
For increased compatibility, replace the config file provided here with a new configuration based on your system's current configuration.
In this case, you must use a config tool like `make menuconfig` to set the `preemption model` to `Fully Preemptible Kernel (RT)`.
For more information, also consult [Stackoverflow](https://stackoverflow.com/questions/51669724/install-rt-linux-patch-for-ubuntu) and the [Panda manuals](https://frankaemika.github.io/docs/installation_linux.html#setting-up-the-real-time-kernel).

## Installation

- **⚠ ATTENTION** Make sure secureboot is disabled. Currently, the compiled kernel is unsigned and thus, will not work with secureboot.
- **⚠ ATTENTION** The repository does not contain the kernel packges, they are built by the CI and and can be downloaded.
- Download the latest kernel packages built by the CI [[as individual files]](https://git-st.inf.tu-dresden.de/ceti/realtime-kernel/-/jobs/artifacts/master/browse?job=build-kernel) [[as archive]](https://git-st.inf.tu-dresden.de/ceti/realtime-kernel/-/jobs/artifacts/master/download?job=build-kernel).
- Install all three packages using `sudo dpkg -i <package-file-name>`.

## Verifying that the Realtime Kernel is Running

- Reboot after kernel installation, selecting the new kernel in the boot loader.
- Run `uname -a`, the result should contain the kernel version number from the installed `deb` package as well as `PREEMPT RT`.

## Set User Permissions

- Add a group named realtime and add the user controlling your robot to this group:
    ```
    sudo addgroup realtime
    sudo usermod -a -G realtime $(whoami)
    ```
- Add the following limits to the realtime group in /etc/security/limits.conf:
    ```
    @realtime soft rtprio 99
    @realtime soft priority 99
    @realtime soft memlock 102400
    @realtime hard rtprio 99
    @realtime hard priority 99
    @realtime hard memlock 102400
    ```
- Log off and on again to let the group assignment and realtime priorities take effect (or, to be safe, restart). Use the command `groups` to check if the the current user belongs to the realtime group.

