#!/bin/bash

# get current kernel and realtime patch version

FILE=`curl -s https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/5.4/`
KERNEL=`echo $FILE | sed -n 's/.*patch-5.4.\(.*\)-rt.*.patch.gz.*/\1/p'`
RTPATCH=`echo $FILE | sed -n 's/.*patch-5.4..*-rt\(.*\).patch.gz.*/\1/p'`
echo "using kernel 5.4.$KERNEL with realtime patch $RTPATCH"

# get kernel file names and location

KERNELURL=https://www.kernel.org/pub/linux/kernel/v5.x/
KERNELNAME=linux-5.4.${KERNEL}
KERNELFILE=linux-5.4.${KERNEL}.tar
KERNELFILEXZ=linux-5.4.${KERNEL}.tar.xz
KERNELSIGN=linux-5.4.${KERNEL}.tar.sign
PATCHURL=https://www.kernel.org/pub/linux/kernel/projects/rt/5.4/
PATCHFILE=patch-5.4.${KERNEL}-rt${RTPATCH}.patch
PATCHFILEXZ=patch-5.4.${KERNEL}-rt${RTPATCH}.patch.xz
PATCHSIGN=patch-5.4.${KERNEL}-rt${RTPATCH}.patch.sign

# download kernel

echo "downloading kernel from ${PATCHURL}${PATCHFILE}"
curl -SLO ${KERNELURL}${KERNELFILEXZ}
curl -SLO ${KERNELURL}${KERNELSIGN}
echo "downloading patch from ${PATCHURL}${PATCHSIGN}"
curl -sLO ${PATCHURL}${PATCHFILEXZ}
curl -sLO ${PATCHURL}${PATCHSIGN}

# unzip kernel (required for verification)

xz -d $KERNELFILEXZ
xz -d $PATCHFILEXZ

# verify the kernel

gpg --verify --keyserver hkp://keys.gnupg.net --auto-key-retrieve $KERNELSIGN
gpg --verify --keyserver hkp://keys.gnupg.net --auto-key-retrieve $PATCHSIGN

# unpack kernel

tar xf $KERNELFILE
cd $KERNELNAME
patch -p1 < ../${PATCHFILE}

# build the kernel

# use the kernel config build on a normal ubuntu 18.4
cp ../config .config
# update config with defaults if new options have been added
make olddefconfig
make deb-pkg
